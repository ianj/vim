
Keeping my vim setup in git as I slowly build it up.
Based on tips found here:

    http://vimcasts.org/episodes/synchronizing-plugins-with-git-submodules-and-pathogen/


Installation:

    git clone https://ianj@bitbucket.org/ianj/vim.git  ~/.vim

Create symlinks:

    ln -s ~/.vim/vimrc ~/.vimrc

Switch to the `~/.vim` directory, and fetch submodules:

    cd ~/.vim
    git submodule init
    git submodule update

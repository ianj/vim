set nocompatible

runtime bundle/pathogen/autoload/pathogen.vim

"Pathogen enables keeping pulugin files in separate folders
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()


set backspace=indent,eol,start

set expandtab
set shiftwidth=4
set tabstop=4

set incsearch
set autoindent

if &t_Co > 2 || has("gui_running")
    syntax on
    set hlsearch
endif


set list
filetype plugin indent on

nmap <C-Up> [e
nmap <C-Down> ]e

vmap <C-Up> [egv
vmap <C-Down> ]egv

set tags=~/project_ctags

colorscheme desert
